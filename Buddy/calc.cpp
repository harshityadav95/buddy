#include "calc.h"
#include "ui_calc.h"
#include<QString>
#include"mainwindow.h"


#include<QMessageBox>
#include<QDesktopServices>
#include<QUrl>
#include<QFileDialog>

QString o="O";
QString a1="A+";
QString a2="A";
QString b1="B+";
QString b2="B";
QString c="C";
QString p="P";
QString f="F";


float sum(float a,float b)
 {
            return((a+b)/15*5);
 }
float sgpa(float m1,float q1,float a1,float m2 ,float q2,float a2,float z3,float att ,float final ,float credit)
{

        float work;

        work=(m1/30)*5;
        work+=((q1+a1)/15*5);
        work+=(m2/6);
        work+=((q2+a2)/15*5);
        work+=(z3/15*5);
        work+=att;
        work+=final;
       // work*=credit;

        return(work);

}




calc::calc(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::calc)
{
    ui->setupUi(this);
}

calc::~calc()
{
    delete ui;
}

void calc::on_pushButton_clicked()
{
   // float result=0.0;
    bool ok;
    float input1[10];
    float input2[10];
    float input3[10];
    float input4[10];
    float input5[10];
    float sums[5];

     QString results1="";
      QString results2="";
       QString results3="";
       QString results4="";
        QString results5="";

    QString grade1="";
     QString grade2="";
      QString grade3="";
       QString grade4="";
        QString grade5="";

        int  gp1a;

        //Subject 1

    input1[0]=ui->lineEdit_9->text().toFloat(&ok);



    if(input1[0]>30)
    {

        QMessageBox::critical(this,tr("Error"),tr("Value Should be Less than 30"));


    }

    input1[1]=ui->lineEdit_14->text().toFloat(&ok);
    input1[2]=ui->lineEdit_34->text().toFloat(&ok);
    input1[3]=ui->lineEdit_19->text().toFloat(&ok);
    input1[4]=ui->lineEdit_24->text().toFloat(&ok);
    input1[5]=ui->lineEdit_39->text().toFloat(&ok);
    input1[6]=ui->lineEdit_44->text().toFloat(&ok);
    input1[7]=ui->lineEdit_49->text().toFloat(&ok);
    input1[8]=ui->lineEdit_54->text().toFloat(&ok);
    input1[9]=ui->lineEdit_59->text().toFloat(&ok);

    sums[0]=sgpa(input1[0],input1[1],input1[2],input1[3],input1[4],input1[5],input1[6],input1[7],input1[8],input1[9]);



    if(sums[0]>=90.0)
    {
        grade1=o;
    }
    else if(sums[0]>=80.0)
    {
        grade1=a1;
    }
    else if(sums[0]>=70.0)
    {
        grade1=a2;
    }
    else if(sums[0]>=60.0)
    {
        grade1=b1;
    }
    else if(sums[0]>=50.0)
    {
        grade1=b2;
    }
    else if(sums[0]>=45.0)
    {
        grade1=c;
    }
    else if(sums[0]>=40.0)
    {
        grade1=p;
    }
    else
    {
        grade1=f;
    }




    ui->lineEdit_64->setText(grade1);

    ui->lineEdit_8->setText(results1.setNum(sums[0]));





    // subject 2

    input2[0]=ui->lineEdit_10->text().toFloat(&ok);
    input2[1]=ui->lineEdit_15->text().toFloat(&ok);
    input2[2]=ui->lineEdit_35->text().toFloat(&ok);
    input2[3]=ui->lineEdit_20->text().toFloat(&ok);
    input2[4]=ui->lineEdit_25->text().toFloat(&ok);
    input2[5]=ui->lineEdit_40->text().toFloat(&ok);
    input2[6]=ui->lineEdit_45->text().toFloat(&ok);
    input2[7]=ui->lineEdit_50->text().toFloat(&ok);
    input2[8]=ui->lineEdit_55->text().toFloat(&ok);
    input2[9]=ui->lineEdit_60->text().toFloat(&ok);

    sums[1]=sgpa(input2[0],input2[1],input2[2],input2[3],input2[4],input2[5],input2[6],input2[7],input2[8],input2[9]);

    if(sums[1]>=90.0)
    {
        grade2=o;
    }
    else if(sums[1]>=80.0)
    {
        grade2=a1;
    }
    else if(sums[1]>=70.0)
    {
        grade2=a2;
    }
    else if(sums[1]>=60.0)
    {
        grade2=b1;
    }
    else if(sums[1]>=50.0)
    {
        grade2=b2;
    }
    else if(sums[1]>=45.0)
    {
        grade2=c;
    }
    else if(sums[1]>=40.0)
    {
        grade2=p;
    }
    else
    {
        grade2=f;
    }

    ui->lineEdit_65->setText(grade2);


    ui->lineEdit_7->setText(results2.setNum(sums[1]));

    // subject 3

    input3[0]=ui->lineEdit_11->text().toFloat(&ok);
    input3[1]=ui->lineEdit_16->text().toFloat(&ok);
    input3[2]=ui->lineEdit_36->text().toFloat(&ok);
    input3[3]=ui->lineEdit_21->text().toFloat(&ok);
    input3[4]=ui->lineEdit_26->text().toFloat(&ok);
    input3[5]=ui->lineEdit_41->text().toFloat(&ok);
    input3[6]=ui->lineEdit_46->text().toFloat(&ok);
    input3[7]=ui->lineEdit_51->text().toFloat(&ok);
    input3[8]=ui->lineEdit_56->text().toFloat(&ok);
    input3[9]=ui->lineEdit_61->text().toFloat(&ok);

    sums[2]=sgpa(input3[0],input3[1],input3[2],input3[3],input3[4],input3[5],input3[6],input3[7],input3[8],input3[9]);


    if(sums[2]>=90.0)
    {
        grade3=o;
    }
    else if(sums[2]>=80.0)
    {
        grade3=a1;
    }
    else if(sums[2]>=70.0)
    {
        grade3=a2;
    }
    else if(sums[2]>=60.0)
    {
        grade3=b1;
    }
    else if(sums[2]>=50.0)
    {
        grade3=b2;
    }
    else if(sums[2]>=45.0)
    {
        grade3=c;
    }
    else if(sums[2]>=40.0)
    {
        grade3=p;
    }
    else
    {
        grade3=f;
    }

    ui->lineEdit_66->setText(grade3);

    ui->lineEdit_6->setText(results3.setNum(sums[2]));

    // subject 4

    input4[0]=ui->lineEdit_12->text().toFloat(&ok);
    input4[1]=ui->lineEdit_17->text().toFloat(&ok);
    input4[2]=ui->lineEdit_37->text().toFloat(&ok);
    input4[3]=ui->lineEdit_22->text().toFloat(&ok);
    input4[4]=ui->lineEdit_27->text().toFloat(&ok);
    input4[5]=ui->lineEdit_42->text().toFloat(&ok);
    input4[6]=ui->lineEdit_47->text().toFloat(&ok);
    input4[7]=ui->lineEdit_52->text().toFloat(&ok);
    input4[8]=ui->lineEdit_57->text().toFloat(&ok);
    input4[9]=ui->lineEdit_62->text().toFloat(&ok);

    sums[3]=sgpa(input4[0],input4[1],input4[2],input4[3],input4[4],input4[5],input4[6],input4[7],input4[8],input4[9]);

    if(sums[3]>=90.0)
    {
        grade4=o;
    }
    else if(sums[3]>=80.0)
    {
        grade4=a1;
    }
    else if(sums[3]>=70.0)
    {
        grade4=a2;
    }
    else if(sums[3]>=60.0)
    {
        grade4=b1;
    }
    else if(sums[3]>=50.0)
    {
        grade4=b2;
    }
    else if(sums[3]>=45.0)
    {
        grade4=c;
    }
    else if(sums[3]>=40.0)
    {
        grade4=p;
    }
    else
    {
        grade4=f;
    }

    ui->lineEdit_67->setText(grade4);

    ui->lineEdit_5->setText(results4.setNum(sums[3]));

    // subject 5

    input5[0]=ui->lineEdit_13->text().toFloat(&ok);


    input5[1]=ui->lineEdit_18->text().toFloat(&ok);
    input5[2]=ui->lineEdit_38->text().toFloat(&ok);
    input5[3]=ui->lineEdit_23->text().toFloat(&ok);
    input5[4]=ui->lineEdit_28->text().toFloat(&ok);
    input5[5]=ui->lineEdit_43->text().toFloat(&ok);
    input5[6]=ui->lineEdit_48->text().toFloat(&ok);
    input5[7]=ui->lineEdit_53->text().toFloat(&ok);
    input5[8]=ui->lineEdit_58->text().toFloat(&ok);
    input5[9]=ui->lineEdit_63->text().toFloat(&ok);

    sums[4]=sgpa(input5[0],input5[1],input5[2],input5[3],input5[4],input5[5],input5[6],input5[7],input5[8],input5[9]);


    if(sums[4]>=90.0)
    {
        grade5=o;
    }
    else if(sums[4]>=80.0)
    {
        grade5=a1;
    }
    else if(sums[4]>=70.0)
    {
        grade5=a2;
    }
    else if(sums[4]>=60.0)
    {
        grade5=b1;
    }
    else if(sums[4]>=50.0)
    {
        grade5=b2;
    }
    else if(sums[4]>=45.0)
    {
        grade5=c;
    }
    else if(sums[4]>=40.0)
    {
        grade5=p;
    }
    else
    {
        grade5=f;
    }

    ui->lineEdit_68->setText(grade5);

 ui->lineEdit_4->setText(results5.setNum(sums[4]));

    gp1a=((sums[0]/10*input1[9])+(sums[1]/10*input2[9])+(sums[2]/10*input3[9])+(sums[3]/10*input4[9])+(sums[4]/10*input5[9]))/25;

     ui->lineEdit->setText(results5.setNum(gp1a));

}

void calc::on_pushButton_2_clicked()
{
    //MainWindow winp;
   // winp.setModal(true);
   // winp.exec();
}
