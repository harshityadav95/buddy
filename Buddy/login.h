#ifndef LOGIN_H
#define LOGIN_H

#include <QDialog>
#include<QtSql>
#include<QDebug>
#include<QFileInfo>
#include"mainwindow.h"
#include <QMainWindow>

namespace Ui {
class login;
}

class login : public QDialog
{
    Q_OBJECT

public:

    QSqlDatabase mydb;
    void connClose()
    {
        mydb.close();
        mydb.removeDatabase(QSqlDatabase::defaultConnection);
    }
    bool connOpen()
    {
        mydb=QSqlDatabase::addDatabase("QSQLITE");
          mydb.setDatabaseName("C:/sqlite/test.db");
           if(mydb.open())
             {
                qDebug()<<("database is ON  ");
               return true;
              }
               else
           {
                   qDebug()<<("Missing  database");
                   return false;

           }
    }

public:
    explicit login(QWidget *parent = 0);
    ~login();

private slots:
    void on_pushButton_clicked();

private:
    Ui::login *ui;
};

#endif // LOGIN_H
