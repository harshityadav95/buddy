#ifndef FACUILTY_H
#define FACUILTY_H

#include <QDialog>
#include"mainwindow.h"

namespace Ui {
class facuilty;
}

class facuilty : public QDialog
{
    Q_OBJECT

public:
    explicit facuilty(QWidget *parent = 0);
    ~facuilty();

private slots:
    void on_pushButton_clicked();

    void on_listView_activated(const QModelIndex &index);

private:
    Ui::facuilty *ui;
};

#endif // FACUILTY_H
