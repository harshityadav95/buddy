#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QtSql>
#include<QDebug>
#include<QFileInfo>
#include"facuilty.h"
#include"about.h"
#include"login.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QSqlDatabase mydb;
    void connClose()
    {
        mydb.close();
        mydb.removeDatabase(QSqlDatabase::defaultConnection);
    }
    bool connOpen()
    {
        mydb=QSqlDatabase::addDatabase("QSQLITE");
          //mydb.setDatabaseName("C:/Users/Harshit Yadav/Documents/buddy/build-Buddy-Desktop_Qt_5_6_0_MSVC2015_64bit-Debug/fac.db");
          //mydb.setDatabaseName("‪‪‪C:\sqlite\test.db");
            mydb.setDatabaseName("C:/sqlite/teacher.db");
           if(mydb.open())
             {
                qDebug()<<("database is ON  ");
               return true;
              }
               else
           {
                   qDebug()<<("Missing  database");
                   return false;

           }
    }


public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
