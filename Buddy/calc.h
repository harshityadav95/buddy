#ifndef CALC_H
#define CALC_H

#include <QDialog>

namespace Ui {
class calc;
}

class calc : public QDialog
{
    Q_OBJECT

public:
    explicit calc(QWidget *parent = 0);
    ~calc();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::calc *ui;
};

#endif // CALC_H
