#include "mainwindow.h"
#include "ui_mainwindow.h"
#include"calc.h"
#include"facuilty.h"
#include"devlopers.h"
#include<QMessageBox>
#include<QDesktopServices>
#include<QUrl>
#include<QFileDialog>
#include"about.h"
#include"login.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    /*QSqlDatabase mydb=QSqlDatabase::addDatabase("QSQLITE");
    mydb.setDatabaseName("Fac.db");
    if(!mydb.open())
    {
        ui->label->setText("Not Conencted  ");
    }
    else
    {
         ui->label->setText(" Conencted  ");
    }*/
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    calc winp;
    winp.setModal(true);
    winp.exec();
}


void MainWindow::on_pushButton_2_clicked()
{
 facuilty won;
    won.setModal(true);
    won.exec();
}

void MainWindow::on_pushButton_3_clicked()
{
    QString link="http://www.lingayasuniversity.edu.in/lingayas/";

    QDesktopServices::openUrl(QUrl(link));
}

void MainWindow::on_pushButton_4_clicked()
{
    QString link="http://174.142.186.87:8081/Login.aspx?ReturnUrl=%2f&AspxAutoDetectCookieSupport=1";

    QDesktopServices::openUrl(QUrl(link));
}

void MainWindow::on_pushButton_5_clicked()
{
    about war;
       war.setModal(true);
       war.exec();
}

void MainWindow::on_pushButton_6_clicked()
{
    Devlopers dead;
       dead.setModal(true);
       dead.exec();
}

void MainWindow::on_pushButton_7_clicked()
{
    login dea;
       dea.setModal(true);
       dea.exec();
}
