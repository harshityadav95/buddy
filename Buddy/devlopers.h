#ifndef DEVLOPERS_H
#define DEVLOPERS_H

#include <QDialog>

namespace Ui {
class Devlopers;
}

class Devlopers : public QDialog
{
    Q_OBJECT

public:
    explicit Devlopers(QWidget *parent = 0);
    ~Devlopers();

private:
    Ui::Devlopers *ui;
};

#endif // DEVLOPERS_H
