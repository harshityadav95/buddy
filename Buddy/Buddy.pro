#-------------------------------------------------
#
# Project created by QtCreator 2016-05-06T00:58:35
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Buddy
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    calc.cpp \
    facuilty.cpp \
    about.cpp \
    devlopers.cpp \
    login.cpp

HEADERS  += mainwindow.h \
    calc.h \
    facuilty.h \
    about.h \
    devlopers.h \
    login.h

FORMS    += mainwindow.ui \
    calc.ui \
    facuilty.ui \
    about.ui \
    devlopers.ui \
    login.ui

RESOURCES += \
    resources.qrc
