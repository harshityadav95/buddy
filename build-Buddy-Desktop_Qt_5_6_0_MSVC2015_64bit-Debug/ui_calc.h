/********************************************************************************
** Form generated from reading UI file 'calc.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CALC_H
#define UI_CALC_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_calc
{
public:
    QPushButton *pushButton;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QLineEdit *lineEdit_8;
    QLineEdit *lineEdit_7;
    QLineEdit *lineEdit_6;
    QLineEdit *lineEdit_5;
    QLineEdit *lineEdit_4;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *lineEdit_9;
    QLineEdit *lineEdit_10;
    QLineEdit *lineEdit_11;
    QLineEdit *lineEdit_12;
    QLineEdit *lineEdit_13;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_3;
    QLineEdit *lineEdit_14;
    QLineEdit *lineEdit_15;
    QLineEdit *lineEdit_16;
    QLineEdit *lineEdit_17;
    QLineEdit *lineEdit_18;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_4;
    QLineEdit *lineEdit_19;
    QLineEdit *lineEdit_20;
    QLineEdit *lineEdit_21;
    QLineEdit *lineEdit_22;
    QLineEdit *lineEdit_23;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_5;
    QLineEdit *lineEdit_24;
    QLineEdit *lineEdit_25;
    QLineEdit *lineEdit_26;
    QLineEdit *lineEdit_27;
    QLineEdit *lineEdit_28;
    QGroupBox *groupBox_7;
    QVBoxLayout *verticalLayout_7;
    QLineEdit *lineEdit_34;
    QLineEdit *lineEdit_35;
    QLineEdit *lineEdit_36;
    QLineEdit *lineEdit_37;
    QLineEdit *lineEdit_38;
    QGroupBox *groupBox_8;
    QVBoxLayout *verticalLayout_8;
    QLineEdit *lineEdit_39;
    QLineEdit *lineEdit_40;
    QLineEdit *lineEdit_41;
    QLineEdit *lineEdit_42;
    QLineEdit *lineEdit_43;
    QGroupBox *groupBox_9;
    QVBoxLayout *verticalLayout_9;
    QLineEdit *lineEdit_44;
    QLineEdit *lineEdit_45;
    QLineEdit *lineEdit_46;
    QLineEdit *lineEdit_47;
    QLineEdit *lineEdit_48;
    QGroupBox *groupBox_10;
    QVBoxLayout *verticalLayout_10;
    QLineEdit *lineEdit_49;
    QLineEdit *lineEdit_50;
    QLineEdit *lineEdit_51;
    QLineEdit *lineEdit_52;
    QLineEdit *lineEdit_53;
    QGroupBox *groupBox_11;
    QVBoxLayout *verticalLayout_11;
    QLineEdit *lineEdit_54;
    QLineEdit *lineEdit_55;
    QLineEdit *lineEdit_56;
    QLineEdit *lineEdit_57;
    QLineEdit *lineEdit_58;
    QGroupBox *groupBox_12;
    QVBoxLayout *verticalLayout_12;
    QLineEdit *lineEdit_59;
    QLineEdit *lineEdit_60;
    QLineEdit *lineEdit_61;
    QLineEdit *lineEdit_62;
    QLineEdit *lineEdit_63;
    QFrame *line;
    QFrame *line_2;
    QFrame *line_3;
    QFrame *line_4;
    QFrame *line_5;
    QFrame *line_6;
    QFrame *line_7;
    QFrame *line_8;
    QFrame *line_10;
    QFrame *line_11;
    QFrame *line_12;
    QFrame *line_13;
    QFrame *line_14;
    QFrame *line_15;
    QFrame *line_16;
    QFrame *line_17;
    QFrame *line_19;
    QFrame *line_20;
    QFrame *line_21;
    QFrame *line_22;
    QFrame *line_23;
    QFrame *line_24;
    QFrame *line_25;
    QFrame *line_26;
    QFrame *line_27;
    QFrame *line_28;
    QFrame *line_29;
    QFrame *line_30;
    QFrame *line_31;
    QFrame *line_32;
    QFrame *line_33;
    QFrame *line_34;
    QFrame *line_35;
    QFrame *line_36;
    QFrame *line_37;
    QFrame *line_38;
    QFrame *line_39;
    QFrame *line_40;
    QFrame *line_41;
    QFrame *line_42;
    QFrame *line_43;
    QFrame *line_44;
    QFrame *line_45;
    QFrame *line_46;
    QFrame *line_47;
    QFrame *line_48;
    QFrame *line_49;
    QFrame *line_50;
    QFrame *line_51;
    QFrame *line_52;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QGroupBox *groupBox_13;
    QVBoxLayout *verticalLayout_13;
    QLineEdit *lineEdit_64;
    QLineEdit *lineEdit_65;
    QLineEdit *lineEdit_66;
    QLineEdit *lineEdit_67;
    QLineEdit *lineEdit_68;
    QFrame *line_9;
    QLineEdit *lineEdit;
    QLabel *label_6;
    QLabel *label_7;

    void setupUi(QDialog *calc)
    {
        if (calc->objectName().isEmpty())
            calc->setObjectName(QStringLiteral("calc"));
        calc->resize(1636, 640);
        calc->setMinimumSize(QSize(1066, 640));
        calc->setStyleSheet(QStringLiteral("background-image: url(:/images/iygf.png);"));
        pushButton = new QPushButton(calc);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(630, 440, 131, 51));
        QFont font;
        font.setPointSize(11);
        font.setBold(true);
        font.setWeight(75);
        pushButton->setFont(font);
        pushButton->setStyleSheet(QLatin1String("background: #64DD17; \n"
"color: white;\n"
" border-radius: 4px;"));
        groupBox = new QGroupBox(calc);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(1090, 160, 71, 261));
        groupBox->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        lineEdit_8 = new QLineEdit(groupBox);
        lineEdit_8->setObjectName(QStringLiteral("lineEdit_8"));
        lineEdit_8->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout->addWidget(lineEdit_8);

        lineEdit_7 = new QLineEdit(groupBox);
        lineEdit_7->setObjectName(QStringLiteral("lineEdit_7"));
        lineEdit_7->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout->addWidget(lineEdit_7);

        lineEdit_6 = new QLineEdit(groupBox);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));
        lineEdit_6->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout->addWidget(lineEdit_6);

        lineEdit_5 = new QLineEdit(groupBox);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));
        lineEdit_5->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout->addWidget(lineEdit_5);

        lineEdit_4 = new QLineEdit(groupBox);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout->addWidget(lineEdit_4);

        groupBox_2 = new QGroupBox(calc);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(80, 160, 81, 261));
        groupBox_2->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        lineEdit_9 = new QLineEdit(groupBox_2);
        lineEdit_9->setObjectName(QStringLiteral("lineEdit_9"));
        lineEdit_9->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_2->addWidget(lineEdit_9);

        lineEdit_10 = new QLineEdit(groupBox_2);
        lineEdit_10->setObjectName(QStringLiteral("lineEdit_10"));
        lineEdit_10->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_2->addWidget(lineEdit_10);

        lineEdit_11 = new QLineEdit(groupBox_2);
        lineEdit_11->setObjectName(QStringLiteral("lineEdit_11"));
        lineEdit_11->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_2->addWidget(lineEdit_11);

        lineEdit_12 = new QLineEdit(groupBox_2);
        lineEdit_12->setObjectName(QStringLiteral("lineEdit_12"));
        lineEdit_12->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_2->addWidget(lineEdit_12);

        lineEdit_13 = new QLineEdit(groupBox_2);
        lineEdit_13->setObjectName(QStringLiteral("lineEdit_13"));
        lineEdit_13->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_2->addWidget(lineEdit_13);

        groupBox_3 = new QGroupBox(calc);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(180, 160, 81, 261));
        groupBox_3->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout_3 = new QVBoxLayout(groupBox_3);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        lineEdit_14 = new QLineEdit(groupBox_3);
        lineEdit_14->setObjectName(QStringLiteral("lineEdit_14"));
        lineEdit_14->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_3->addWidget(lineEdit_14);

        lineEdit_15 = new QLineEdit(groupBox_3);
        lineEdit_15->setObjectName(QStringLiteral("lineEdit_15"));
        lineEdit_15->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_3->addWidget(lineEdit_15);

        lineEdit_16 = new QLineEdit(groupBox_3);
        lineEdit_16->setObjectName(QStringLiteral("lineEdit_16"));
        lineEdit_16->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_3->addWidget(lineEdit_16);

        lineEdit_17 = new QLineEdit(groupBox_3);
        lineEdit_17->setObjectName(QStringLiteral("lineEdit_17"));
        lineEdit_17->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_3->addWidget(lineEdit_17);

        lineEdit_18 = new QLineEdit(groupBox_3);
        lineEdit_18->setObjectName(QStringLiteral("lineEdit_18"));
        lineEdit_18->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_3->addWidget(lineEdit_18);

        groupBox_4 = new QGroupBox(calc);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setGeometry(QRect(380, 160, 81, 261));
        groupBox_4->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout_4 = new QVBoxLayout(groupBox_4);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        lineEdit_19 = new QLineEdit(groupBox_4);
        lineEdit_19->setObjectName(QStringLiteral("lineEdit_19"));
        lineEdit_19->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_4->addWidget(lineEdit_19);

        lineEdit_20 = new QLineEdit(groupBox_4);
        lineEdit_20->setObjectName(QStringLiteral("lineEdit_20"));
        lineEdit_20->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_4->addWidget(lineEdit_20);

        lineEdit_21 = new QLineEdit(groupBox_4);
        lineEdit_21->setObjectName(QStringLiteral("lineEdit_21"));
        lineEdit_21->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_4->addWidget(lineEdit_21);

        lineEdit_22 = new QLineEdit(groupBox_4);
        lineEdit_22->setObjectName(QStringLiteral("lineEdit_22"));
        lineEdit_22->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_4->addWidget(lineEdit_22);

        lineEdit_23 = new QLineEdit(groupBox_4);
        lineEdit_23->setObjectName(QStringLiteral("lineEdit_23"));
        lineEdit_23->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_4->addWidget(lineEdit_23);

        groupBox_5 = new QGroupBox(calc);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setGeometry(QRect(480, 160, 81, 261));
        groupBox_5->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout_5 = new QVBoxLayout(groupBox_5);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        lineEdit_24 = new QLineEdit(groupBox_5);
        lineEdit_24->setObjectName(QStringLiteral("lineEdit_24"));
        lineEdit_24->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_5->addWidget(lineEdit_24);

        lineEdit_25 = new QLineEdit(groupBox_5);
        lineEdit_25->setObjectName(QStringLiteral("lineEdit_25"));
        lineEdit_25->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_5->addWidget(lineEdit_25);

        lineEdit_26 = new QLineEdit(groupBox_5);
        lineEdit_26->setObjectName(QStringLiteral("lineEdit_26"));
        lineEdit_26->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_5->addWidget(lineEdit_26);

        lineEdit_27 = new QLineEdit(groupBox_5);
        lineEdit_27->setObjectName(QStringLiteral("lineEdit_27"));
        lineEdit_27->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_5->addWidget(lineEdit_27);

        lineEdit_28 = new QLineEdit(groupBox_5);
        lineEdit_28->setObjectName(QStringLiteral("lineEdit_28"));
        lineEdit_28->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_5->addWidget(lineEdit_28);

        groupBox_7 = new QGroupBox(calc);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        groupBox_7->setGeometry(QRect(280, 160, 81, 261));
        groupBox_7->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout_7 = new QVBoxLayout(groupBox_7);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        lineEdit_34 = new QLineEdit(groupBox_7);
        lineEdit_34->setObjectName(QStringLiteral("lineEdit_34"));
        lineEdit_34->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_7->addWidget(lineEdit_34);

        lineEdit_35 = new QLineEdit(groupBox_7);
        lineEdit_35->setObjectName(QStringLiteral("lineEdit_35"));
        lineEdit_35->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_7->addWidget(lineEdit_35);

        lineEdit_36 = new QLineEdit(groupBox_7);
        lineEdit_36->setObjectName(QStringLiteral("lineEdit_36"));
        lineEdit_36->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_7->addWidget(lineEdit_36);

        lineEdit_37 = new QLineEdit(groupBox_7);
        lineEdit_37->setObjectName(QStringLiteral("lineEdit_37"));
        lineEdit_37->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_7->addWidget(lineEdit_37);

        lineEdit_38 = new QLineEdit(groupBox_7);
        lineEdit_38->setObjectName(QStringLiteral("lineEdit_38"));
        lineEdit_38->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_7->addWidget(lineEdit_38);

        groupBox_8 = new QGroupBox(calc);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        groupBox_8->setGeometry(QRect(580, 160, 81, 261));
        groupBox_8->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout_8 = new QVBoxLayout(groupBox_8);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        lineEdit_39 = new QLineEdit(groupBox_8);
        lineEdit_39->setObjectName(QStringLiteral("lineEdit_39"));
        lineEdit_39->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_8->addWidget(lineEdit_39);

        lineEdit_40 = new QLineEdit(groupBox_8);
        lineEdit_40->setObjectName(QStringLiteral("lineEdit_40"));
        lineEdit_40->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_8->addWidget(lineEdit_40);

        lineEdit_41 = new QLineEdit(groupBox_8);
        lineEdit_41->setObjectName(QStringLiteral("lineEdit_41"));
        lineEdit_41->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_8->addWidget(lineEdit_41);

        lineEdit_42 = new QLineEdit(groupBox_8);
        lineEdit_42->setObjectName(QStringLiteral("lineEdit_42"));
        lineEdit_42->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_8->addWidget(lineEdit_42);

        lineEdit_43 = new QLineEdit(groupBox_8);
        lineEdit_43->setObjectName(QStringLiteral("lineEdit_43"));
        lineEdit_43->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_8->addWidget(lineEdit_43);

        groupBox_9 = new QGroupBox(calc);
        groupBox_9->setObjectName(QStringLiteral("groupBox_9"));
        groupBox_9->setGeometry(QRect(680, 160, 81, 261));
        groupBox_9->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout_9 = new QVBoxLayout(groupBox_9);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        lineEdit_44 = new QLineEdit(groupBox_9);
        lineEdit_44->setObjectName(QStringLiteral("lineEdit_44"));
        lineEdit_44->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_9->addWidget(lineEdit_44);

        lineEdit_45 = new QLineEdit(groupBox_9);
        lineEdit_45->setObjectName(QStringLiteral("lineEdit_45"));
        lineEdit_45->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_9->addWidget(lineEdit_45);

        lineEdit_46 = new QLineEdit(groupBox_9);
        lineEdit_46->setObjectName(QStringLiteral("lineEdit_46"));
        lineEdit_46->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_9->addWidget(lineEdit_46);

        lineEdit_47 = new QLineEdit(groupBox_9);
        lineEdit_47->setObjectName(QStringLiteral("lineEdit_47"));
        lineEdit_47->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_9->addWidget(lineEdit_47);

        lineEdit_48 = new QLineEdit(groupBox_9);
        lineEdit_48->setObjectName(QStringLiteral("lineEdit_48"));
        lineEdit_48->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_9->addWidget(lineEdit_48);

        groupBox_10 = new QGroupBox(calc);
        groupBox_10->setObjectName(QStringLiteral("groupBox_10"));
        groupBox_10->setGeometry(QRect(780, 160, 81, 261));
        groupBox_10->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout_10 = new QVBoxLayout(groupBox_10);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        lineEdit_49 = new QLineEdit(groupBox_10);
        lineEdit_49->setObjectName(QStringLiteral("lineEdit_49"));
        lineEdit_49->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_10->addWidget(lineEdit_49);

        lineEdit_50 = new QLineEdit(groupBox_10);
        lineEdit_50->setObjectName(QStringLiteral("lineEdit_50"));
        lineEdit_50->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_10->addWidget(lineEdit_50);

        lineEdit_51 = new QLineEdit(groupBox_10);
        lineEdit_51->setObjectName(QStringLiteral("lineEdit_51"));
        lineEdit_51->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_10->addWidget(lineEdit_51);

        lineEdit_52 = new QLineEdit(groupBox_10);
        lineEdit_52->setObjectName(QStringLiteral("lineEdit_52"));
        lineEdit_52->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_10->addWidget(lineEdit_52);

        lineEdit_53 = new QLineEdit(groupBox_10);
        lineEdit_53->setObjectName(QStringLiteral("lineEdit_53"));
        lineEdit_53->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_10->addWidget(lineEdit_53);

        groupBox_11 = new QGroupBox(calc);
        groupBox_11->setObjectName(QStringLiteral("groupBox_11"));
        groupBox_11->setGeometry(QRect(880, 160, 81, 261));
        groupBox_11->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout_11 = new QVBoxLayout(groupBox_11);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        lineEdit_54 = new QLineEdit(groupBox_11);
        lineEdit_54->setObjectName(QStringLiteral("lineEdit_54"));
        lineEdit_54->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_11->addWidget(lineEdit_54);

        lineEdit_55 = new QLineEdit(groupBox_11);
        lineEdit_55->setObjectName(QStringLiteral("lineEdit_55"));
        lineEdit_55->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_11->addWidget(lineEdit_55);

        lineEdit_56 = new QLineEdit(groupBox_11);
        lineEdit_56->setObjectName(QStringLiteral("lineEdit_56"));
        lineEdit_56->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_11->addWidget(lineEdit_56);

        lineEdit_57 = new QLineEdit(groupBox_11);
        lineEdit_57->setObjectName(QStringLiteral("lineEdit_57"));
        lineEdit_57->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_11->addWidget(lineEdit_57);

        lineEdit_58 = new QLineEdit(groupBox_11);
        lineEdit_58->setObjectName(QStringLiteral("lineEdit_58"));
        lineEdit_58->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        verticalLayout_11->addWidget(lineEdit_58);

        groupBox_12 = new QGroupBox(calc);
        groupBox_12->setObjectName(QStringLiteral("groupBox_12"));
        groupBox_12->setGeometry(QRect(980, 160, 81, 261));
        groupBox_12->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout_12 = new QVBoxLayout(groupBox_12);
        verticalLayout_12->setObjectName(QStringLiteral("verticalLayout_12"));
        lineEdit_59 = new QLineEdit(groupBox_12);
        lineEdit_59->setObjectName(QStringLiteral("lineEdit_59"));
        lineEdit_59->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        lineEdit_59->setMaxLength(1);
        lineEdit_59->setReadOnly(false);
        lineEdit_59->setClearButtonEnabled(false);

        verticalLayout_12->addWidget(lineEdit_59);

        lineEdit_60 = new QLineEdit(groupBox_12);
        lineEdit_60->setObjectName(QStringLiteral("lineEdit_60"));
        lineEdit_60->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        lineEdit_60->setMaxLength(1);

        verticalLayout_12->addWidget(lineEdit_60);

        lineEdit_61 = new QLineEdit(groupBox_12);
        lineEdit_61->setObjectName(QStringLiteral("lineEdit_61"));
        lineEdit_61->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        lineEdit_61->setMaxLength(1);

        verticalLayout_12->addWidget(lineEdit_61);

        lineEdit_62 = new QLineEdit(groupBox_12);
        lineEdit_62->setObjectName(QStringLiteral("lineEdit_62"));
        lineEdit_62->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        lineEdit_62->setMaxLength(1);

        verticalLayout_12->addWidget(lineEdit_62);

        lineEdit_63 = new QLineEdit(groupBox_12);
        lineEdit_63->setObjectName(QStringLiteral("lineEdit_63"));
        lineEdit_63->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        lineEdit_63->setMaxLength(1);

        verticalLayout_12->addWidget(lineEdit_63);

        line = new QFrame(calc);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(160, 200, 21, 20));
        line->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        line_2 = new QFrame(calc);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(160, 240, 21, 20));
        line_2->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        line_3 = new QFrame(calc);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setGeometry(QRect(160, 290, 21, 20));
        line_3->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);
        line_4 = new QFrame(calc);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setGeometry(QRect(160, 330, 21, 20));
        line_4->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);
        line_5 = new QFrame(calc);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setGeometry(QRect(160, 370, 21, 20));
        line_5->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);
        line_6 = new QFrame(calc);
        line_6->setObjectName(QStringLiteral("line_6"));
        line_6->setGeometry(QRect(260, 200, 21, 20));
        line_6->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_6->setFrameShape(QFrame::HLine);
        line_6->setFrameShadow(QFrame::Sunken);
        line_7 = new QFrame(calc);
        line_7->setObjectName(QStringLiteral("line_7"));
        line_7->setGeometry(QRect(260, 240, 21, 20));
        line_7->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_7->setFrameShape(QFrame::HLine);
        line_7->setFrameShadow(QFrame::Sunken);
        line_8 = new QFrame(calc);
        line_8->setObjectName(QStringLiteral("line_8"));
        line_8->setGeometry(QRect(260, 290, 21, 20));
        line_8->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_8->setFrameShape(QFrame::HLine);
        line_8->setFrameShadow(QFrame::Sunken);
        line_10 = new QFrame(calc);
        line_10->setObjectName(QStringLiteral("line_10"));
        line_10->setGeometry(QRect(260, 330, 21, 20));
        line_10->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_10->setFrameShape(QFrame::HLine);
        line_10->setFrameShadow(QFrame::Sunken);
        line_11 = new QFrame(calc);
        line_11->setObjectName(QStringLiteral("line_11"));
        line_11->setGeometry(QRect(260, 370, 21, 20));
        line_11->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_11->setFrameShape(QFrame::HLine);
        line_11->setFrameShadow(QFrame::Sunken);
        line_12 = new QFrame(calc);
        line_12->setObjectName(QStringLiteral("line_12"));
        line_12->setGeometry(QRect(360, 200, 21, 20));
        line_12->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_12->setFrameShape(QFrame::HLine);
        line_12->setFrameShadow(QFrame::Sunken);
        line_13 = new QFrame(calc);
        line_13->setObjectName(QStringLiteral("line_13"));
        line_13->setGeometry(QRect(360, 240, 21, 20));
        line_13->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_13->setFrameShape(QFrame::HLine);
        line_13->setFrameShadow(QFrame::Sunken);
        line_14 = new QFrame(calc);
        line_14->setObjectName(QStringLiteral("line_14"));
        line_14->setGeometry(QRect(360, 290, 21, 20));
        line_14->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_14->setFrameShape(QFrame::HLine);
        line_14->setFrameShadow(QFrame::Sunken);
        line_15 = new QFrame(calc);
        line_15->setObjectName(QStringLiteral("line_15"));
        line_15->setGeometry(QRect(360, 330, 21, 20));
        line_15->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_15->setFrameShape(QFrame::HLine);
        line_15->setFrameShadow(QFrame::Sunken);
        line_16 = new QFrame(calc);
        line_16->setObjectName(QStringLiteral("line_16"));
        line_16->setGeometry(QRect(360, 370, 21, 20));
        line_16->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_16->setFrameShape(QFrame::HLine);
        line_16->setFrameShadow(QFrame::Sunken);
        line_17 = new QFrame(calc);
        line_17->setObjectName(QStringLiteral("line_17"));
        line_17->setGeometry(QRect(460, 200, 21, 20));
        line_17->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_17->setFrameShape(QFrame::HLine);
        line_17->setFrameShadow(QFrame::Sunken);
        line_19 = new QFrame(calc);
        line_19->setObjectName(QStringLiteral("line_19"));
        line_19->setGeometry(QRect(460, 240, 21, 20));
        line_19->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_19->setFrameShape(QFrame::HLine);
        line_19->setFrameShadow(QFrame::Sunken);
        line_20 = new QFrame(calc);
        line_20->setObjectName(QStringLiteral("line_20"));
        line_20->setGeometry(QRect(460, 290, 21, 20));
        line_20->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_20->setFrameShape(QFrame::HLine);
        line_20->setFrameShadow(QFrame::Sunken);
        line_21 = new QFrame(calc);
        line_21->setObjectName(QStringLiteral("line_21"));
        line_21->setGeometry(QRect(460, 330, 21, 20));
        line_21->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_21->setFrameShape(QFrame::HLine);
        line_21->setFrameShadow(QFrame::Sunken);
        line_22 = new QFrame(calc);
        line_22->setObjectName(QStringLiteral("line_22"));
        line_22->setGeometry(QRect(460, 370, 21, 20));
        line_22->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_22->setFrameShape(QFrame::HLine);
        line_22->setFrameShadow(QFrame::Sunken);
        line_23 = new QFrame(calc);
        line_23->setObjectName(QStringLiteral("line_23"));
        line_23->setGeometry(QRect(560, 200, 21, 20));
        line_23->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_23->setFrameShape(QFrame::HLine);
        line_23->setFrameShadow(QFrame::Sunken);
        line_24 = new QFrame(calc);
        line_24->setObjectName(QStringLiteral("line_24"));
        line_24->setGeometry(QRect(560, 240, 21, 20));
        line_24->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_24->setFrameShape(QFrame::HLine);
        line_24->setFrameShadow(QFrame::Sunken);
        line_25 = new QFrame(calc);
        line_25->setObjectName(QStringLiteral("line_25"));
        line_25->setGeometry(QRect(560, 290, 21, 20));
        line_25->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_25->setFrameShape(QFrame::HLine);
        line_25->setFrameShadow(QFrame::Sunken);
        line_26 = new QFrame(calc);
        line_26->setObjectName(QStringLiteral("line_26"));
        line_26->setGeometry(QRect(560, 330, 21, 20));
        line_26->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_26->setFrameShape(QFrame::HLine);
        line_26->setFrameShadow(QFrame::Sunken);
        line_27 = new QFrame(calc);
        line_27->setObjectName(QStringLiteral("line_27"));
        line_27->setGeometry(QRect(560, 370, 21, 20));
        line_27->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_27->setFrameShape(QFrame::HLine);
        line_27->setFrameShadow(QFrame::Sunken);
        line_28 = new QFrame(calc);
        line_28->setObjectName(QStringLiteral("line_28"));
        line_28->setGeometry(QRect(660, 200, 21, 20));
        line_28->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_28->setFrameShape(QFrame::HLine);
        line_28->setFrameShadow(QFrame::Sunken);
        line_29 = new QFrame(calc);
        line_29->setObjectName(QStringLiteral("line_29"));
        line_29->setGeometry(QRect(660, 240, 21, 21));
        line_29->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_29->setFrameShape(QFrame::HLine);
        line_29->setFrameShadow(QFrame::Sunken);
        line_30 = new QFrame(calc);
        line_30->setObjectName(QStringLiteral("line_30"));
        line_30->setGeometry(QRect(660, 290, 21, 20));
        line_30->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_30->setFrameShape(QFrame::HLine);
        line_30->setFrameShadow(QFrame::Sunken);
        line_31 = new QFrame(calc);
        line_31->setObjectName(QStringLiteral("line_31"));
        line_31->setGeometry(QRect(660, 330, 21, 20));
        line_31->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_31->setFrameShape(QFrame::HLine);
        line_31->setFrameShadow(QFrame::Sunken);
        line_32 = new QFrame(calc);
        line_32->setObjectName(QStringLiteral("line_32"));
        line_32->setGeometry(QRect(660, 370, 21, 20));
        line_32->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_32->setFrameShape(QFrame::HLine);
        line_32->setFrameShadow(QFrame::Sunken);
        line_33 = new QFrame(calc);
        line_33->setObjectName(QStringLiteral("line_33"));
        line_33->setGeometry(QRect(760, 200, 21, 20));
        line_33->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_33->setFrameShape(QFrame::HLine);
        line_33->setFrameShadow(QFrame::Sunken);
        line_34 = new QFrame(calc);
        line_34->setObjectName(QStringLiteral("line_34"));
        line_34->setGeometry(QRect(760, 240, 21, 20));
        line_34->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_34->setFrameShape(QFrame::HLine);
        line_34->setFrameShadow(QFrame::Sunken);
        line_35 = new QFrame(calc);
        line_35->setObjectName(QStringLiteral("line_35"));
        line_35->setGeometry(QRect(760, 290, 21, 20));
        line_35->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_35->setFrameShape(QFrame::HLine);
        line_35->setFrameShadow(QFrame::Sunken);
        line_36 = new QFrame(calc);
        line_36->setObjectName(QStringLiteral("line_36"));
        line_36->setGeometry(QRect(760, 330, 21, 20));
        line_36->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_36->setFrameShape(QFrame::HLine);
        line_36->setFrameShadow(QFrame::Sunken);
        line_37 = new QFrame(calc);
        line_37->setObjectName(QStringLiteral("line_37"));
        line_37->setGeometry(QRect(760, 370, 21, 20));
        line_37->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_37->setFrameShape(QFrame::HLine);
        line_37->setFrameShadow(QFrame::Sunken);
        line_38 = new QFrame(calc);
        line_38->setObjectName(QStringLiteral("line_38"));
        line_38->setGeometry(QRect(860, 200, 21, 20));
        line_38->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_38->setFrameShape(QFrame::HLine);
        line_38->setFrameShadow(QFrame::Sunken);
        line_39 = new QFrame(calc);
        line_39->setObjectName(QStringLiteral("line_39"));
        line_39->setGeometry(QRect(860, 240, 21, 20));
        line_39->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_39->setFrameShape(QFrame::HLine);
        line_39->setFrameShadow(QFrame::Sunken);
        line_40 = new QFrame(calc);
        line_40->setObjectName(QStringLiteral("line_40"));
        line_40->setGeometry(QRect(860, 290, 21, 20));
        line_40->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_40->setFrameShape(QFrame::HLine);
        line_40->setFrameShadow(QFrame::Sunken);
        line_41 = new QFrame(calc);
        line_41->setObjectName(QStringLiteral("line_41"));
        line_41->setGeometry(QRect(860, 330, 21, 20));
        line_41->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_41->setFrameShape(QFrame::HLine);
        line_41->setFrameShadow(QFrame::Sunken);
        line_42 = new QFrame(calc);
        line_42->setObjectName(QStringLiteral("line_42"));
        line_42->setGeometry(QRect(860, 370, 21, 20));
        line_42->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_42->setFrameShape(QFrame::HLine);
        line_42->setFrameShadow(QFrame::Sunken);
        line_43 = new QFrame(calc);
        line_43->setObjectName(QStringLiteral("line_43"));
        line_43->setGeometry(QRect(960, 200, 21, 20));
        line_43->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_43->setFrameShape(QFrame::HLine);
        line_43->setFrameShadow(QFrame::Sunken);
        line_44 = new QFrame(calc);
        line_44->setObjectName(QStringLiteral("line_44"));
        line_44->setGeometry(QRect(960, 240, 21, 20));
        line_44->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_44->setFrameShape(QFrame::HLine);
        line_44->setFrameShadow(QFrame::Sunken);
        line_45 = new QFrame(calc);
        line_45->setObjectName(QStringLiteral("line_45"));
        line_45->setGeometry(QRect(960, 290, 21, 20));
        line_45->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_45->setFrameShape(QFrame::HLine);
        line_45->setFrameShadow(QFrame::Sunken);
        line_46 = new QFrame(calc);
        line_46->setObjectName(QStringLiteral("line_46"));
        line_46->setGeometry(QRect(960, 330, 21, 20));
        line_46->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_46->setFrameShape(QFrame::HLine);
        line_46->setFrameShadow(QFrame::Sunken);
        line_47 = new QFrame(calc);
        line_47->setObjectName(QStringLiteral("line_47"));
        line_47->setGeometry(QRect(960, 370, 21, 20));
        line_47->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_47->setFrameShape(QFrame::HLine);
        line_47->setFrameShadow(QFrame::Sunken);
        line_48 = new QFrame(calc);
        line_48->setObjectName(QStringLiteral("line_48"));
        line_48->setGeometry(QRect(1060, 200, 31, 20));
        line_48->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_48->setFrameShape(QFrame::HLine);
        line_48->setFrameShadow(QFrame::Sunken);
        line_49 = new QFrame(calc);
        line_49->setObjectName(QStringLiteral("line_49"));
        line_49->setGeometry(QRect(1060, 240, 31, 20));
        line_49->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_49->setFrameShape(QFrame::HLine);
        line_49->setFrameShadow(QFrame::Sunken);
        line_50 = new QFrame(calc);
        line_50->setObjectName(QStringLiteral("line_50"));
        line_50->setGeometry(QRect(1060, 280, 31, 20));
        line_50->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_50->setFrameShape(QFrame::HLine);
        line_50->setFrameShadow(QFrame::Sunken);
        line_51 = new QFrame(calc);
        line_51->setObjectName(QStringLiteral("line_51"));
        line_51->setGeometry(QRect(1060, 330, 31, 20));
        line_51->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_51->setFrameShape(QFrame::HLine);
        line_51->setFrameShadow(QFrame::Sunken);
        line_52 = new QFrame(calc);
        line_52->setObjectName(QStringLiteral("line_52"));
        line_52->setGeometry(QRect(1060, 370, 31, 20));
        line_52->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_52->setFrameShape(QFrame::HLine);
        line_52->setFrameShadow(QFrame::Sunken);
        label = new QLabel(calc);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 200, 71, 16));
        label->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        label_2 = new QLabel(calc);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 240, 71, 16));
        label_2->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        label_3 = new QLabel(calc);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 290, 71, 16));
        label_3->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        label_4 = new QLabel(calc);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 330, 71, 16));
        label_4->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        label_5 = new QLabel(calc);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 370, 71, 16));
        label_5->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        groupBox_13 = new QGroupBox(calc);
        groupBox_13->setObjectName(QStringLiteral("groupBox_13"));
        groupBox_13->setGeometry(QRect(1210, 160, 81, 261));
        groupBox_13->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout_13 = new QVBoxLayout(groupBox_13);
        verticalLayout_13->setObjectName(QStringLiteral("verticalLayout_13"));
        lineEdit_64 = new QLineEdit(groupBox_13);
        lineEdit_64->setObjectName(QStringLiteral("lineEdit_64"));
        lineEdit_64->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        lineEdit_64->setMaxLength(1);
        lineEdit_64->setReadOnly(false);
        lineEdit_64->setClearButtonEnabled(false);

        verticalLayout_13->addWidget(lineEdit_64);

        lineEdit_65 = new QLineEdit(groupBox_13);
        lineEdit_65->setObjectName(QStringLiteral("lineEdit_65"));
        lineEdit_65->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        lineEdit_65->setMaxLength(1);

        verticalLayout_13->addWidget(lineEdit_65);

        lineEdit_66 = new QLineEdit(groupBox_13);
        lineEdit_66->setObjectName(QStringLiteral("lineEdit_66"));
        lineEdit_66->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        lineEdit_66->setMaxLength(1);

        verticalLayout_13->addWidget(lineEdit_66);

        lineEdit_67 = new QLineEdit(groupBox_13);
        lineEdit_67->setObjectName(QStringLiteral("lineEdit_67"));
        lineEdit_67->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        lineEdit_67->setMaxLength(1);

        verticalLayout_13->addWidget(lineEdit_67);

        lineEdit_68 = new QLineEdit(groupBox_13);
        lineEdit_68->setObjectName(QStringLiteral("lineEdit_68"));
        lineEdit_68->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        lineEdit_68->setMaxLength(1);

        verticalLayout_13->addWidget(lineEdit_68);

        line_9 = new QFrame(calc);
        line_9->setObjectName(QStringLiteral("line_9"));
        line_9->setGeometry(QRect(1240, 420, 20, 31));
        line_9->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        line_9->setFrameShape(QFrame::VLine);
        line_9->setFrameShadow(QFrame::Sunken);
        lineEdit = new QLineEdit(calc);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(1210, 450, 91, 31));
        lineEdit->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        label_6 = new QLabel(calc);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(1100, 460, 101, 21));
        label_6->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        label_7 = new QLabel(calc);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(580, 110, 281, 16));
        label_7->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        retranslateUi(calc);

        QMetaObject::connectSlotsByName(calc);
    } // setupUi

    void retranslateUi(QDialog *calc)
    {
        calc->setWindowTitle(QApplication::translate("calc", "Dialog", 0));
        pushButton->setText(QApplication::translate("calc", "Calculate ", 0));
        groupBox->setTitle(QApplication::translate("calc", "Total ", 0));
        groupBox_2->setTitle(QApplication::translate("calc", "Mid Term -1 ", 0));
        groupBox_3->setTitle(QApplication::translate("calc", "Quiz -1", 0));
        groupBox_4->setTitle(QApplication::translate("calc", "Mid Term -2", 0));
        groupBox_5->setTitle(QApplication::translate("calc", "Quiz - 2", 0));
        groupBox_7->setTitle(QApplication::translate("calc", "Assignment - 1", 0));
        groupBox_8->setTitle(QApplication::translate("calc", "Assignment - 2", 0));
        groupBox_9->setTitle(QApplication::translate("calc", "Quiz/Assig. -3", 0));
        groupBox_10->setTitle(QApplication::translate("calc", "Attendance", 0));
        groupBox_11->setTitle(QApplication::translate("calc", "Final", 0));
        groupBox_12->setTitle(QApplication::translate("calc", "Credit", 0));
        lineEdit_59->setText(QApplication::translate("calc", "3", 0));
        lineEdit_60->setText(QApplication::translate("calc", "4", 0));
        lineEdit_61->setText(QApplication::translate("calc", "3", 0));
        lineEdit_62->setText(QApplication::translate("calc", "3", 0));
        lineEdit_63->setText(QApplication::translate("calc", "4", 0));
        label->setText(QApplication::translate("calc", "CJ", 0));
        label_2->setText(QApplication::translate("calc", "CN", 0));
        label_3->setText(QApplication::translate("calc", "CG", 0));
        label_4->setText(QApplication::translate("calc", "DAC", 0));
        label_5->setText(QApplication::translate("calc", "POM", 0));
        groupBox_13->setTitle(QApplication::translate("calc", "Grade", 0));
        lineEdit_64->setText(QString());
        lineEdit_65->setText(QString());
        lineEdit_66->setText(QString());
        lineEdit_67->setText(QString());
        lineEdit_68->setText(QString());
        label_6->setText(QApplication::translate("calc", "<html><head/><body><p><span style=\" font-size:10pt;\">Estimated SGPA -</span></p></body></html>", 0));
        label_7->setText(QApplication::translate("calc", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-weight:600;\">SGPA Calculator</span></p></body></html>", 0));
    } // retranslateUi

};

namespace Ui {
    class calc: public Ui_calc {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CALC_H
