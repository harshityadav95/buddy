/********************************************************************************
** Form generated from reading UI file 'about.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ABOUT_H
#define UI_ABOUT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_about
{
public:
    QLabel *label;
    QLabel *label_2;

    void setupUi(QDialog *about)
    {
        if (about->objectName().isEmpty())
            about->setObjectName(QStringLiteral("about"));
        about->resize(1066, 640);
        about->setMinimumSize(QSize(1066, 640));
        about->setStyleSheet(QStringLiteral("background-image: url(:/images/pack3.png);"));
        label = new QLabel(about);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(410, 120, 278, 68));
        label->setStyleSheet(QLatin1String("background: rgb(255, 255, 255); \n"
"font-family: Good Times;"));
        label_2 = new QLabel(about);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(30, 200, 1001, 201));
        label_2->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        label_2->setScaledContents(false);
        label_2->setWordWrap(true);

        retranslateUi(about);

        QMetaObject::connectSlotsByName(about);
    } // setupUi

    void retranslateUi(QDialog *about)
    {
        about->setWindowTitle(QApplication::translate("about", "About BUDDY ", 0));
        label->setText(QApplication::translate("about", "<html><head/><body><p><span style=\" font-size:48pt;\">Buddy</span></p></body></html>", 0));
        label_2->setText(QApplication::translate("about", "<html><head/><body><p align=\"justify\"><span style=\" font-size:11pt;\">Buddy Is an Expert System Software that has being Devloped for the Educational Sector . Unlike the Traditional age old ERP systems that are implemented by the Management and contains all the tools and resources which are out of the domain of the student Access . The Buddy is designed to work as Productivity Solution for Students through which they can increase their performance in Academics and Co-currical activites as well manage their Student Life in a smart and efficient way .</span></p><p align=\"center\"><span style=\" font-size:11pt;\">This Version of BUDDY is customised as per the Academic Guidlines Of Lingayas University .</span></p><p><span style=\" font-size:11pt;\"><br/></span></p><p align=\"right\"><span style=\" font-size:11pt;\">-- Devlopment Team</span></p><p><br/></p></body></html>", 0));
    } // retranslateUi

};

namespace Ui {
    class about: public Ui_about {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ABOUT_H
