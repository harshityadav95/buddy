/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QLabel *label_2;
    QPushButton *pushButton_7;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1066, 640);
        MainWindow->setMinimumSize(QSize(1066, 640));
        MainWindow->setMaximumSize(QSize(910, 16777215));
        MainWindow->setLayoutDirection(Qt::LeftToRight);
        MainWindow->setAutoFillBackground(false);
        MainWindow->setStyleSheet(QLatin1String("background-image: url(:/images/iygf.png);\n"
"\n"
"background-repeat:no-repeat;"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(840, 240, 171, 61));
        QFont font;
        font.setPointSize(11);
        pushButton->setFont(font);
        pushButton->setStyleSheet(QLatin1String("background: #64DD17; \n"
"color: white;\n"
" border-radius: 4px;"));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(840, 360, 171, 61));
        pushButton_2->setFont(font);
        pushButton_2->setStyleSheet(QLatin1String("background: rgb(255, 87, 34); \n"
"color: white;\n"
" border-radius: 4px;"));
        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(30, 240, 171, 61));
        QFont font1;
        font1.setPointSize(10);
        pushButton_3->setFont(font1);
        pushButton_3->setStyleSheet(QLatin1String("background: rgb(156, 39, 176); \n"
"color: white;\n"
" border-radius: 4px;"));
        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(30, 360, 171, 61));
        pushButton_4->setFont(font);
        pushButton_4->setStyleSheet(QLatin1String("background: rgb(121, 85, 72); \n"
"color: white;\n"
" border-radius: 4px;"));
        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(30, 130, 171, 61));
        QFont font2;
        font2.setPointSize(12);
        pushButton_5->setFont(font2);
        pushButton_5->setStyleSheet(QLatin1String("background: rgb(255, 193, 7); \n"
"color: white;\n"
" border-radius: 4px;"));
        pushButton_6 = new QPushButton(centralWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setGeometry(QRect(840, 130, 171, 61));
        QFont font3;
        font3.setFamily(QStringLiteral("Calibri"));
        font3.setPointSize(12);
        pushButton_6->setFont(font3);
        pushButton_6->setStyleSheet(QLatin1String("background: rgb(33, 150, 243);\n"
"color: white;\n"
" border-radius: 4px;"));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(380, 120, 271, 271));
        label_2->setPixmap(QPixmap(QString::fromUtf8("ty.png")));
        label_2->setScaledContents(true);
        pushButton_7 = new QPushButton(centralWidget);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));
        pushButton_7->setGeometry(QRect(440, 460, 181, 41));
        QFont font4;
        font4.setPointSize(14);
        font4.setBold(true);
        font4.setWeight(75);
        pushButton_7->setFont(font4);
        pushButton_7->setStyleSheet(QLatin1String("background: rgb(0, 145, 234); \n"
"color: white;\n"
" border-radius: 4px;"));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1066, 20));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Buddy 1.0", 0));
        pushButton->setText(QApplication::translate("MainWindow", "Calculator", 0));
        pushButton_2->setText(QApplication::translate("MainWindow", "Teachers", 0));
        pushButton_3->setText(QApplication::translate("MainWindow", "Open Website", 0));
        pushButton_4->setText(QApplication::translate("MainWindow", "ERP ", 0));
        pushButton_5->setText(QApplication::translate("MainWindow", "About Us", 0));
        pushButton_6->setText(QApplication::translate("MainWindow", "Devlopers", 0));
        label_2->setText(QString());
        pushButton_7->setText(QApplication::translate("MainWindow", "Login", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
