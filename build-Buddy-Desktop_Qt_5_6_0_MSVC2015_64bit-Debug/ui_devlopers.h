/********************************************************************************
** Form generated from reading UI file 'devlopers.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEVLOPERS_H
#define UI_DEVLOPERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_Devlopers
{
public:
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;

    void setupUi(QDialog *Devlopers)
    {
        if (Devlopers->objectName().isEmpty())
            Devlopers->setObjectName(QStringLiteral("Devlopers"));
        Devlopers->resize(1066, 640);
        Devlopers->setMinimumSize(QSize(1066, 640));
        Devlopers->setStyleSheet(QStringLiteral("background-image: url(:/images/pack3.png);"));
        label = new QLabel(Devlopers);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(270, 160, 511, 291));
        label->setPixmap(QPixmap(QString::fromUtf8(":/images/2016-27-5--01-22-05.png")));
        label->setScaledContents(true);
        label_2 = new QLabel(Devlopers);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(290, 460, 461, 31));
        label_2->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        label_3 = new QLabel(Devlopers);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(280, 120, 461, 31));
        label_3->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));

        retranslateUi(Devlopers);

        QMetaObject::connectSlotsByName(Devlopers);
    } // setupUi

    void retranslateUi(QDialog *Devlopers)
    {
        Devlopers->setWindowTitle(QApplication::translate("Devlopers", "Dialog", 0));
        label->setText(QString());
        label_2->setText(QApplication::translate("Devlopers", "<html><head/><body><p align=\"center\"><span style=\" font-size:11pt;\">Soham Kundu ( 14CS015 ) &amp; Harshit Yadav ( 14CS003)</span></p></body></html>", 0));
        label_3->setText(QApplication::translate("Devlopers", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-weight:600;\">Devloped By </span></p></body></html>", 0));
    } // retranslateUi

};

namespace Ui {
    class Devlopers: public Ui_Devlopers {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEVLOPERS_H
