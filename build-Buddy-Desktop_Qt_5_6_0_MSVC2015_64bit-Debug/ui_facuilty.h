/********************************************************************************
** Form generated from reading UI file 'facuilty.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FACUILTY_H
#define UI_FACUILTY_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_facuilty
{
public:
    QLabel *label;
    QPushButton *pushButton;
    QListView *listView;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QLabel *label_8;
    QLineEdit *lineEdit_6;
    QLabel *label_10;
    QLineEdit *lineEdit_9;
    QLabel *label_11;
    QLineEdit *lineEdit_8;
    QLabel *label_7;
    QLineEdit *lineEdit_10;
    QLabel *label_9;
    QLineEdit *lineEdit_7;
    QLabel *label_2;
    QFrame *line;

    void setupUi(QDialog *facuilty)
    {
        if (facuilty->objectName().isEmpty())
            facuilty->setObjectName(QStringLiteral("facuilty"));
        facuilty->resize(1066, 640);
        facuilty->setMinimumSize(QSize(1066, 640));
        facuilty->setStyleSheet(QStringLiteral("background-image: url(:/images/pack3.png);"));
        label = new QLabel(facuilty);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 160, 161, 41));
        label->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        pushButton = new QPushButton(facuilty);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(760, 410, 141, 51));
        pushButton->setStyleSheet(QLatin1String("background: rgb(156, 39, 176); \n"
"color: white;\n"
" border-radius: 4px;"));
        listView = new QListView(facuilty);
        listView->setObjectName(QStringLiteral("listView"));
        listView->setGeometry(QRect(30, 210, 161, 231));
        listView->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        groupBox = new QGroupBox(facuilty);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(380, 140, 271, 311));
        groupBox->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));

        verticalLayout->addWidget(label_8);

        lineEdit_6 = new QLineEdit(groupBox);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));

        verticalLayout->addWidget(lineEdit_6);

        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QStringLiteral("label_10"));

        verticalLayout->addWidget(label_10);

        lineEdit_9 = new QLineEdit(groupBox);
        lineEdit_9->setObjectName(QStringLiteral("lineEdit_9"));

        verticalLayout->addWidget(lineEdit_9);

        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QStringLiteral("label_11"));

        verticalLayout->addWidget(label_11);

        lineEdit_8 = new QLineEdit(groupBox);
        lineEdit_8->setObjectName(QStringLiteral("lineEdit_8"));

        verticalLayout->addWidget(lineEdit_8);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        verticalLayout->addWidget(label_7);

        lineEdit_10 = new QLineEdit(groupBox);
        lineEdit_10->setObjectName(QStringLiteral("lineEdit_10"));

        verticalLayout->addWidget(lineEdit_10);

        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));

        verticalLayout->addWidget(label_9);

        lineEdit_7 = new QLineEdit(groupBox);
        lineEdit_7->setObjectName(QStringLiteral("lineEdit_7"));

        verticalLayout->addWidget(lineEdit_7);

        label_2 = new QLabel(facuilty);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(670, 150, 321, 231));
        label_2->setStyleSheet(QStringLiteral("background: rgb(255, 255, 255); "));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/images/rep.png")));
        label_2->setScaledContents(true);
        line = new QFrame(facuilty);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(190, 310, 191, 16));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        retranslateUi(facuilty);

        QMetaObject::connectSlotsByName(facuilty);
    } // setupUi

    void retranslateUi(QDialog *facuilty)
    {
        facuilty->setWindowTitle(QApplication::translate("facuilty", "Dialog", 0));
        label->setText(QApplication::translate("facuilty", "Select The Facuilty ", 0));
        pushButton->setText(QApplication::translate("facuilty", "Update Database ", 0));
        groupBox->setTitle(QApplication::translate("facuilty", "Facuilty Details", 0));
        label_8->setText(QApplication::translate("facuilty", "Name", 0));
        label_10->setText(QApplication::translate("facuilty", "Designation ", 0));
        label_11->setText(QApplication::translate("facuilty", "Room Number ", 0));
        label_7->setText(QApplication::translate("facuilty", "Contact Number", 0));
        label_9->setText(QApplication::translate("facuilty", "Email Address ", 0));
        label_2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class facuilty: public Ui_facuilty {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FACUILTY_H
